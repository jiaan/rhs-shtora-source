
class CfgFunctions
{
	class RHS
	{
		tag = "RHS";
		class SHTORA
		{
			class shtoraInit
			{
				file = "\rhsafrf\addons\rhs_aps\shtora\rhs_shtoraInit.sqf";
				description = "Shtora init";
			};
			class shtoraAngle
			{
				file = "\rhsafrf\addons\rhs_aps\shtora\rhs_shtoraAngle.sqf";
				description = "Shtora angle";
			};
			class shtoraDetect
			{
				file = "\rhsafrf\addons\rhs_aps\shtora\rhs_shtoraDetect.sqf";
				description = "Shtora detect";
			};
			class shtoraController
			{
				file = "\rhsafrf\addons\rhs_aps\shtora\rhs_shtoraController.sqf";
				description = "Shtora controller";
			};
			class shtoraInput
			{
				file = "\rhsafrf\addons\rhs_aps\shtora\rhs_shtoraInput.sqf";
				description = "Shtora input";
			};
			class shtoraTurn
			{
				file = "\rhsafrf\addons\rhs_aps\shtora\rhs_shtoraTurn.sqf";
				description = "Shtora turn";
			};
			class shtoraCounter
			{
				file = "\rhsafrf\addons\rhs_aps\shtora\rhs_shtoraCounter.sqf";
				description = "Shtora countermesures";
			};
			class shtoraCounterMP
			{
				file = "\rhsafrf\addons\rhs_aps\shtora\rhs_shtoraCounterMP.sqf";
				description = "Shtora countermesures MP";
			};
			class shtoraSound
			{
				file = "\rhsafrf\addons\rhs_aps\shtora\rhs_shtoraSound.sqf";
				description = "Shtora sound";
			};
			class shtoraDeflect
			{
				file = "\rhsafrf\addons\rhs_aps\shtora\rhs_shtoraDeflect.sqf";
				description = "Shtora deflect";
			};
			class shtoraIncomingMissileEH
			{
				file = "\rhsafrf\addons\rhs_aps\shtora\rhs_shtoraIncomingMissileEH.sqf";
				description = "Shtora incoming missile event handler";
			};
			class shtoraReset
			{
				file = "\rhsafrf\addons\rhs_aps\shtora\rhs_shtoraReset.sqf";
				description = "Shtora reset local variables";
			};
		};
	};
};

class CfgRemoteExec
{
	class Functions
	{
		mode = 2;
		jip = 1;
		class RHS_fnc_shtoraInput
		{
			allowedTargets = 0;
		};
		class RHS_fnc_shtoraTurn
		{
			allowedTargets = 0;
		};
		class RHS_fnc_shtoraCounter
		{
			allowedTargets = 0;
		};
		class RHS_fnc_shtoraCounterMP
		{
			allowedTargets = 0;
		};
		class RHS_fnc_shtoraSound
		{
			allowedTargets = 0;
		};
		class RHS_fnc_shtoraDeflect
		{
			allowedTargets = 0;
		};
	};
};
