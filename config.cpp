#include "script_component.hpp"

class CfgPatches
{
	class rhs_aps
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"rhs_main"};
		author = $STR_RHS_AUTHOR_FULL;
		url = "http://www.rhsmods.org/";
		authorUrl = "http://redhammer.su";
		versionDesc = "RHS";
		//version = VERSION;
		text = QUOTE(RHS_TAG VERSION);
	};
};


#include "cfgVehicles.hpp"

#include "cfgFunctions.hpp"

class CfgSounds
{
	class RHS_APS_Warning
	{
		name = "APS_warning";
		sound[] =
		{
			"\rhsafrf\addons\rhs_aps\sound\warning.wav",
			db - 15,
			1
		};
		titles[] = {};
	};
};