# RHS Shtora Source

This is the source code for the [Shtora system](https://en.wikipedia.org/wiki/Shtora-1) simulation in the [RHS mod](http://www.rhsmods.org) for ARMA 3.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/teGbWGtIxXI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
