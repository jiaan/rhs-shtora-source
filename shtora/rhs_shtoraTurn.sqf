
private ["_r","_o","_p"];

params ["_v","_m","_a"];
// Vehicle object
// Mode scalar
// Animation array

// Radians & Offset & Position
_r = rad(_a select 0);
_o = _a select 1;
_p = _a select 2;

// Turn if offset is greater than 3 degrees and turret not damaged
if (_o > 3 && {_v getHitPointDamage "HitTurret" < 0.7}) then {
	private ["_g","_d","_ai"];
	// Get vehicle gunner
	_g = gunner _v;
	// Exit if SHTORA is already TURNING
	if (_v getVariable ["RHS_SHTORA_TURNING", false]) exitWith {};
	// Set SHTORA TURNING to true over network
	_v setVariable ["RHS_SHTORA_TURNING", true, true];
	// Animation time
	_t = 0;
	// Is the gunner AI?
	if (call RHS_fnc_findPlayer == _g || !(alive _g)) then {
		// Player / dead animation
		// Animation time
		_t = 12 / 360 * _o;
		// Do animations
		_v animate ["ShtoraTurret", _r];
		_v animate ["ShtoraOptics", _r];
		// Set AI to false
		_ai = false;
	} else {
		// Animation time
		_t = 12 / 360 * _o;
		// Do watch for AI gunners
		_g disableAI "TARGET";
		_g disableAI "AUTOTARGET";
		_g doWatch [
			(_p select 0),
			(_p select 1),
			(getPosATL _v) select 2
		];
		// Set AI to true
		_ai = true;
	};
	// Wait for animation to complete
	sleep _t;
	// Enable AI
	if (_ai && {alive _g}) then {
		_g enableAI "TARGET";
		_g enableAI "AUTOTARGET";
		_g doWatch objNull;
	};
	// Set SHTORA TURNING to false over network
	if (alive _v) then {
		_v setVariable ["RHS_SHTORA_TURNING", false, true];
	};
};

// Exit if SHTORA is already TURNING
if (_v getVariable ["RHS_SHTORA_TURNING", false]) exitWith {};

// Fire countermeasures if alive
if (alive _v) then {
	[_v, true, true] remoteExec ["RHS_fnc_shtoraCounter", _v];
};
