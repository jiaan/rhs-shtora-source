private ["_m","_pos","_exit"];

// Input
params ["_t","_s","_p","_a"];
// Target object
// Shooter object
// Projectile object
// Ammo string (default empty)

// Exit if shooter is not local
if !(local _s) exitWith {false};

// Exit if target SHTORA is not ENABLED or turret is damaged
if !(_t getVariable ["RHS_SHTORA_ENABLED", false]) exitWith {false};
if (_t getHitPointDamage "HitTurret" >= 0.7) exitWith {false};

// Get target SHTORA mode and exit if OFF(0)
_m = _t getVariable ["RHS_SHTORA_MODE", 0];
if (_m < 1) exitWith {false};

private ["_pos","_exit","_tPosAGL","_tPosASL","_sVec","_tVec","_rAzi","_rEle","_arr","_ang"];

// Get shooter eye position
_pos = [];
if !(isNull (gunner _s)) then {
	_pos = eyePos (gunner _s);
} else {
	_pos = eyePos _s;
};

// Check LOS between shooter and target sensors
// Get the relative angle and offset of animation
_exit = false;
for "_i" from 1 to 6 do {
	// Get the sensor's ATL & ASL positions
	_tPosAGL = _t modelToWorld (_t selectionPosition ["aps_s" + str _i, "Memory"]);
	_tPosASL = AGLToASL _tPosAGL;
	// If there is a good LOS between target and shooter
	if !(terrainIntersectASL [_tPosASL, _pos]) then {
		if !(lineIntersects [_tPosASL, _pos, vehicle _s]) then {
			// Get target/shooter vector, sensor vector and the relative azimuth
			_sVec = _tPosASL vectorFromTo _pos;
			_tVec = _tPosAGL vectorFromTo (_t modelToWorld (_t selectionPosition ["aps_s" + str _i + "_dir", "Memory"]));
			_rAzi = aCos((_sVec select 0) * (_tVec select 0) + (_sVec select 1) * (_tVec select 1));
			// If azimuth is within 90 degrees
			if (_rAzi <= 45) then {
				// Get elevation between vectors
				_rEle = abs(aSin(_sVec select 2) - aSin(_tVec select 2));
				// If elevation is within 30 degrees
				if (_rEle <= 22.5) then {
					// Get turn angle and add margin of error
					_arr = [_t,_s] call RHS_fnc_shtoraAngle;
					_ang = _arr select 0;
					if (_i > 2) then { // 7.5 degrees for sides
						_ang = _ang + (7.5 - random(15));
					} else { // 3.75 for front
						_ang = _ang + (3.75 - random(7.5));
					};
					if (_ang > 180) then { _ang = -(360 - _ang) };
					if (_ang < -180) then { _ang = 360 + _ang };
					_arr set [0, _ang];
					// Good LOS, pass to controller with angle to animate
					[_t, _s, _p, _m, _arr, _a] call RHS_fnc_shtoraController;
					// Exit loop
					_exit = true;
				};
			};
		};
	};
	// Exit loop
	if (_exit) exitWith {};
};

// Return result
_exit;