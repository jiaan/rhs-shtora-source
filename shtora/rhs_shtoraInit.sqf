// Exit if module is disabled
if !(missionNameSpace getVariable ["RHS_SHTORA_ENABLED", true]) exitWith {};

// Input
params ["_vehicle"];	// Vehicle object

// Exit if vehicle module is disabled
if !(_vehicle getVariable ["RHS_SHTORA_ENABLED", true]) exitWith {};

// Exit if vehicle doesn't have SHTORA
if (getNumber(configFile >> "cfgVehicles" >> typeOf _vehicle >> "RHS_APS_SHTORA") < 1) exitWith {};

// Add event handlers
_vehicle addEventHandler ["Killed", {
	params ["_vehicle"];
	_vehicle setVariable ["RHS_SHTORA_ENABLED", false];
}];

// Set SHTORA loacl variables
_vehicle setVariable ["RHS_SHTORA_DAZZLER_CAMERA", false];
_vehicle setVariable ["RHS_SHTORA_DAZZLER_NVG", false];
_vehicle setVariable ["RHS_SHTORA_DAZZLER_FLIR", false];
_vehicle setVariable ["RHS_SHTORA_DAZZLER_LIGHTS", []];

if (_vehicle isKindOf "rhs_t80uk") then {
	_vehicle setVariable ["RHS_SHTORA_DAZZLER_SELECTION", 7];
} else {
	_vehicle setVariable ["RHS_SHTORA_DAZZLER_SELECTION", 7];
};

// Run only where this vehicle is local
if !(local _vehicle) exitWith {};

// Set SHTORA public variables
_vehicle setVariable ["RHS_SHTORA_ENABLED", true, true];
_vehicle setVariable ["RHS_SHTORA_DAMAGE", 0, true];
_vehicle setVariable ["RHS_SHTORA_TURNING", false, true];
_vehicle setVariable ["RHS_SHTORA_DAZZLE", false, true];
_vehicle setVariable ["RHS_SHTORA_SMOKE", false, true];

if (isNull (commander _vehicle)) then {
	_vehicle setVariable ["RHS_SHTORA_MODE", 0, true]; // OFF(0) - SEMI(1) - AUTO(2)
} else {
	_vehicle setVariable ["RHS_SHTORA_MODE", 1, true]; // OFF(0) - SEMI(1) - AUTO(2)
};

// Loop while vehicle is alive
while {_vehicle getVariable ["RHS_SHTORA_ENABLED", false]} do {
	// Reset offset for AI gunner
	private _gunner = gunner _vehicle;
	if (!isNull _gunner) then {
		if !(_gunner call RHS_fnc_isPlayer) then {
			_vehicle animate ["ShtoraTurret", 0];
			_vehicle animate ["ShtoraOptics", 0];
		};
	};
	// Link MODE to AI behaviour
	private _commander = commander _vehicle;
	if (alive _commander) then {
		if !(_commander call RHS_fnc_isPlayer) then {
			private _mode = switch (behaviour _commander) do {
				case "CARELESS": {0};
				case "SAFE": {1};
				case "AWARE": {2};
				case "COMBAT": {2};
				case "STEALTH": {1};
				default {0};
			};
			if (_mode != _vehicle getVariable ["RHS_SHTORA_MODE", 0]) then {
				_vehicle setVariable ["RHS_SHTORA_MODE", _mode, true];
			};
		};
	};
	sleep 3;
};