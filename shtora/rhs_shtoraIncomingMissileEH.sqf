
params ["_target","_ammo","_shooter"];

// Exit if missile is not SACLOS
if (getNumber(configFile >> "cfgAmmo" >> _ammo >> "rhs_saclos") == 0) exitWith {};

// Get gunner
_shooter = gunner _shooter;

// Exit if vehicle gunner is not local
if !(local _shooter) exitWith {};

// Exit if module is disabled
if !(missionNameSpace getVariable ["RHS_SHTORA_ENABLED", true]) exitWith {};

// Get missiles object
private _missile = nearestObject [_shooter, _ammo];
if (isNull _missile) exitWith {};

// Notify target's Shtora system 
[_target, _shooter, _missile, "SACLOS"] call rhs_fnc_shtoraDetect;