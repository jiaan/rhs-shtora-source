private ["_v","_s""_tdir","_dir","_pha","_off","_ang","_pos"];

params ["_v","_s"];
// Vehicle object
// Shooter object.

// Get shooter position
private _sPos = getPosASL _s;

// Get turret direction
private _tDir = _v weaponDirection ((weapons _v) select 0);
_tDir = ((_tDir select 0) atan2 (_tDir select 1) + 360) % 360;

// Get direction from turret to shooter
private _rDir = (getPosASL _v) vectorFromTo _sPos;
_rDir = ((_rDir select 0) atan2 (_rDir select 1) + 360) % 360;

// Get Shtora animation offset
private _sDir = deg(_v animationPhase "ShtoraTurret");

// Turn required & offset
private _turn = _tDir - _rDir;
private _offset = _turn;
_turn = _turn + _sDir;

// Normalize
if (_turn > 180) then { _turn = -(360 - _turn) };
if (_turn < -180) then { _turn = 360 + _turn };
if (_offset > 180) then { _offset = -(360 - _offset) };
if (_offset < -180) then { _offset = 360 + _offset };
_offset = abs(_offset);

// Return turn angle, offset and shooter 2d position
[
	_turn,
	_offset,
	[_sPos select 0, _sPos select 1]
];