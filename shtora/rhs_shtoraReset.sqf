/*
	Name: 		RHS_fnc_effectDazzler

	Desc:		Control dazzler effects

	Param(s):	Vehicle object
				NVG bool
				Camera bool
				FLIR bool

	Returns:	null

	Author:		Jiaan "Bakerman"
*/


params ["_vehicle"];

_vehicle setVariable ["RHS_SHTORA_DAZZLER_CAMERA", false];
_vehicle setVariable ["RHS_SHTORA_DAZZLER_NVG", false];
_vehicle setVariable ["RHS_SHTORA_DAZZLER_FLIR", false];
{
	detach _x;
	deleteVehicle _x;
} count (_vehicle getVariable ["RHS_SHTORA_DAZZLER_LIGHTS",[]]);
_vehicle setVariable ["RHS_SHTORA_DAZZLER_LIGHTS", []];