// Input
params ["_v","_s","_p","_m","_a","_t"];
// Vehicle object
// Shooter object
// Projectile object
// Mode scalar
// Animation array

// Play alert sound for all crew members
{
	if (alive _x && _x call RHS_fnc_isPlayer) then {
		["RHS_APS_Warning",_forEachIndex * 2 + 1, 14, 0.35] remoteExec ["RHS_fnc_shtoraSound", _x];
	};
} forEach [driver _v, gunner _v, commander _v];

// Do action
if (_m > 1) then {
	// MODE AUTO
	// Turn turret where the gunner or vehicle is local
	private _owner = gunner _v;
	if (isNull _owner) then {_owner = _v};
	[_v,_m,_a] remoteExec ["RHS_fnc_shtoraTurn", _owner];
} else {
	// MODE SEMI-AUTO
	// Get input where the commander or vehicle is local
	private _owner = commander _v;
	if (isNull _owner) then {_owner = _v};
	[_v,_m,_a] remoteExec ["RHS_fnc_shtoraInput", _owner];
};

// Deflection
if (toLower _t == "saclos") then {
	[_v,_s,_p] spawn RHS_fnc_shtoraDeflect;
};