// Input
params ["_v","_s","_p"];
// Vehicle object
// Shooter object
// Projectile object

// Track only once
private _var = "RHS_APS_DEFLECTING_" + str _p;
if (missionNamespace getVariable [_var, false]) exitWith {};
missionNamespace setVariable [_var, true];

// Get ammo cmImmunity & derive base deflection power
private _ammo = typeOf _p;
private _cmImmunity = getNumber(configFile >> "cfgAmmo" >> _ammo >> "cmImmunity");
private _flared = false;
private _power = 5 + 20 * (1 - _cmImmunity);
private _ang = 0;

// Interfere with guidance
private _interfere = {
	params ["_p","_power"];
	// Get pitch, bank and yaw
	private _pitchBank = _p call BIS_fnc_getPitchBank;
	private _pitch = _pitchBank select 0;
	private _bank = _pitchBank select 1;
	private _yaw = getDir _p;
	// Greatest effect on pitch
	_pitch = _pitch + (_power * 1.2) - random ((_power * 1.2) * 2);
	if (_pitch > 90) then { _pitch = 90; };
	if (_pitch < -90) then { _pitch = -90; };
	// Slight bank adjustments
	_bank = _bank + (_power * 0.5) - random ((_power * 0.5) * 2);
	if (_bank > 180) then { _bank = 180; };
	if (_bank < -180) then { _bank = -180; };
	// Slight yaw adjustments
	_yaw = _yaw + (_power * 0.7) - random ((_power * 0.7) * 2);
	_yaw = _yaw % 360; 
	// Set new pitch, bank and yaw
	[_p,[_yaw,_pitch,_bank]] call bis_fnc_setobjectrotation;
};

// While projectile is alive
while {!isNull _p && {!isNull _v} && {!isNull _s}} do {
	// Loop at 10hz
	sleep 0.1;
	// Get relative angle for turrets
	_ang = ([_v,_s] call RHS_fnc_shtoraAngle) select 1;
	if (!_flared && {_ang < 100}) then {
		// IR countermeasures interference, only once
		if (_v getVariable ["RHS_SHTORA_SMOKE", false]) then {
			[_p,random (15 * (1 - _cmImmunity))] call _interfere;
			_flared = true;
		};
	};
	if (_ang < 45) then {
		// Dazzler interference
		if (_v getVariable ["RHS_SHTORA_DAZZLE", false]) then {
			if (random 1 > _cmImmunity) then {
				[_p,_power / 40 * (45 - _ang)] call _interfere;
			};
		};
	};
};

// Remove variable
missionNamespace setVariable [_var, nil];