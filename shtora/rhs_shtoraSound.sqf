
// Input
params ["_s","_v",["_r", 1],["_d", 0]];

// Play sound if in first person view
for "_x" from 0 to _r do {
	if (vehicle (call RHS_fnc_findPlayer) getVariable ["RHS_SHTORA_ENABLED", true]) then {
		if (cameraView != "EXTERNAL") then {
			for "_i" from 0 to _v do {
				playSound [_s, true];
			};
		};
	};
	sleep _d;
};

