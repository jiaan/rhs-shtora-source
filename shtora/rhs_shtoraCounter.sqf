
private ["_g","_c","_w","_dNull","_sNull"];

params ["_v",["_d", true],["_s", true]];
// Vehicle object
// Dazzle boolean
// Smoke boolean

// Get the vehicle's gunner
_g = gunner _v;

// Get the vehicle's commander
_c = commander _v;

// Null fire
_dNull = false;
_sNull = false;

// Fire dazzler
if (_d) then {
	if (alive _c) then {
		if (_c call RHS_fnc_isPlayer) then {
			// For players
			_v action ["UseWeapon", _v, _c, 0];
		} else {
			// For AI
			_w = _v currentWeaponTurret [0,0];
			_v selectWeaponTurret ["rhs_weap_dazzler", [0,0]];
			_v fire "rhs_weap_dazzler";
			sleep 0.1;
			_v selectWeaponTurret [_w, [0,0]];
		};
	} else {
		// Null fire dazzler
		_dNull = true;
	};
};

private _shtoraSmoke = {
	params ["_v"];
		if (_v ammo "rhs_weap_902b" > 0) then {
		_v setVariable ["RHS_SHTORA_SMOKE", true, true];
		_v spawn {
			sleep 5;
			_this setVariable ["RHS_SHTORA_SMOKE", false, true];
		};
	};
};

// Fire smoke
if (_s) then {
	if (alive _g) then {
		if (_g call RHS_fnc_isPlayer) then {
			// For players
			_v action ["UseWeapon", _v, _g, 11];
			_v call _shtoraSmoke;
		} else {
			// Fire command for AI / empty
			_v call _shtoraSmoke;
			_w = currentWeapon _v;
			for "_i" from 1 to 6 do {
				_v fireAtTarget [objNull, "rhs_weap_902b"];
				sleep 0.1;
			};
			_v selectWeapon _w;
		};
	} else {
		// Null fire smoke
		_sNull = true;
	};
};

// Null fire over network
if (_dNull || _sNull) then {
	if (!_dNull && _sNull) then {
		// Fire smoke only if vehicle has ammo
		if (_v ammo "rhs_weap_902b" > 0) then {
			_v call _shtoraSmoke;
			[_v, _dNull, _sNull] remoteExec ["rhs_fnc_shtoraCounterMP", 0];
		};
	} else {
		[_v, _dNull, _sNull] remoteExec ["rhs_fnc_shtoraCounterMP", 0];
	};
};