private ["_c","_animate"];

// Input
params ["_v","_m","_a",["_o", 5]];
// Vehicle object
// Mode scalar
// Animation array
// Time out scalar (default 5 seconds)

// Get vehicle commander
_c = commander _v;

// Exit if commander is dead/null, rerun if new player commander enters
if !(alive _c) exitWith {
	_t = 0;
	// Wait for new commander or time out
	while {_t <= _o - 1 && alive _v} do {
		_c = commander _v;
		if (alive _c && _c call RHS_fnc_isPlayer) exitWith {
			[_v,_m,_a,_o-_t] remoteExec ["RHS_fnc_shtoraInput", _c];
		};
		_t = _t + 0.1;
		sleep 0.1;
	};
};

// Do animation
_animate = false;

// If the commander is the player and alive
if (call RHS_fnc_findPlayer == _c && {hasInterface}) then {
	private ["_e","_key","_keys","_t"];
	// Show key prompt hint
	if (count (actionKeys "CarHandBrake") < 1) then {
		hint ("SHTORA: Press " + '"X"' + " to rotate turret");
	} else {
		hint ("SHTORA: Press " + actionKeysNames "CarHandBrake" + " to rotate turret");
	};
	// Wait for display or time out
	_t = 0;
	waitUntil {!(isNull(findDisplay 46))};
	// Set user interface to wait for key
	uiNamespace setVariable ['RHS_SHTORA_KEY', false];
	// Key event handler
	_e = (findDisplay 46) displayAddEventHandler ["KeyDown", {
		_key = _this select 1;
		_keys = actionKeys "CarHandBrake";
		// Default to X(45) if no handbrake key is set
		if (count _keys < 1) then {_keys = [45]};
		{
			if (_x == _key) exitWith {
				uiNamespace setVariable ["RHS_SHTORA_KEY", true];
			};
		} forEach _keys;
		false;
	}];
	// Wait until key is pressed of time out
	_t = 0;
	while {_t <= _o && alive _v} do {
		// Exit on key press
		if (uiNamespace getVariable ['RHS_SHTORA_KEY', false]) exitWith {
			_animate = true;
		};
		// Exit if the commander has changed, rerun this function for new player commander
		if (_c != commander _v) exitWith {
			_c = commander _v;
			if (alive _c && _c call RHS_fnc_isPlayer) exitWith {
				[_v,_m,_a,_o-_t] remoteExec ["RHS_fnc_shtoraInput", _c];
			};
		};
		_t = _t + 0.1;
		sleep 0.1;
	};
	// Remove event handler and key prompt
	if (!isNull (findDisplay 46)) then {
		(findDisplay 46) displayRemoveEventHandler ["KeyDown", _e];
	};
	hint "";
} else {
	// Sleep while AI commander ponders on the thought of pressing the button
	sleep (random 2 + 0.5);
	// Do animation
	_animate = true;
};

// Do animation or fire
if (_animate) then {
	private "_owner";
	// Turn turret where the gunner or vehicle is local
	_owner = gunner _v;
	if (isNull _owner) then {_owner = _v};
	[_v,_m,_a] remoteExec ["RHS_fnc_shtoraTurn", _owner];
};