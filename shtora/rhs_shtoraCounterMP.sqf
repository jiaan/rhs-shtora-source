params ["_v",["_d", true],["_s", true]];
// Vehicle object
// Dazzle boolean
// Smoke boolean

// Fire dazzler
if (_d) then {
	[_v, "rhs_weap_dazzler"] call rhs_fnc_effectFiredDazzler;
};

// Fire smoke
if (_s) then {
	// Get locality
	private ["_local","_ammo"];
	_local = local _v;
	_ammo = _v ammo "rhs_weap_902b";
	if (_ammo > 0) then {
		if (_ammo > 6) then { _ammo = 6 };
		for "_i" from 1 to _ammo do {
			[_v, "rhs_weap_902b", nil, nil, "rhs_ammo_3d17_902B", "rhs_mag_3d17_12"] call rhs_fnc_effectFiredSmokeLauncher;
			if (_local) then {
				_v setAmmo ["rhs_weap_902b", (_v ammo "rhs_weap_902b") - 1];
				playSound3D ["A3\Sounds_F\arsenal\weapons\UGL\UGL_01.wss", _v];
			};
			sleep 0.075;
		};
	};
};